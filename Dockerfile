FROM docker:1.11

MAINTAINER Alexis PIRES <pires.alexis@gmail.com>

# Add docker compose
RUN apk add --update --repository http://cdn.alpinelinux.org/alpine/edge/community py-pip
RUN pip install docker-compose

CMD ["docker-compose", "--version"]