# Introduction

Image wich contains both docker client and docker-compose but does not contains dind.

- Docker version: 1.11
- Docker compose version: 1.6.2
- OS: Alpine 3.4 **or** Ubuntu 14.04


## Tags
- *latest, alpine*: 41 MB ([Dockerfile](https://gitlab.com/angry-smoothie/docker-compose/blob/master/Dockerfile))
- *ubuntu*: 165MB ([Dockerfile](https://gitlab.com/angry-smoothie/docker-compose/blob/ubuntu/Dockerfile))

Check our [Gitlab repo](https://gitlab.com/groups/angry-smoothie).